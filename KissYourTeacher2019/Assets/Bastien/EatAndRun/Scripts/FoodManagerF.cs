﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodManagerF : MonoBehaviour
{
    public static FoodManagerF Current;

    public GameObject HealthBar;
    public GameObject foodBar;
    public GameObject sugarBar;
    public GameObject waterBar;
    public GameObject scoreNumber;


    // Start is called before the first frame update
    void Awake()
    {
        Current = this;
    }
}
