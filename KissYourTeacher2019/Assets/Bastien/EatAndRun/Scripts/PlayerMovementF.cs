using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovementF : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    Rigidbody rb;
    public float jumpForce;
    public bool isGrounded = true;
    private Quaternion qTo;
    public Animator animator;
    public bool isRunning;


    int screenshotIndex=0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        qTo = transform.rotation;
        animator = gameObject.GetComponentInChildren<Animator>();
        //animator.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.RightArrow))
            animator.SetBool("isMoving", true);
        else animator.SetBool("isMoving", false);


        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(horizontal * Time.deltaTime * speed, 0, vertical * Time.deltaTime * speed);
        //Vector3 direction = new Vector3(horizontal, 0, vertical);
        //gameObject.transform.Translate(direction.normalized * Time.deltaTime * speed);
        rb.MovePosition(transform.position + movement);
        //transform.rotation = Quaternion.LookRotation(movement);
        if (movement != Vector3.zero) qTo = Quaternion.LookRotation(movement);
        transform.rotation = Quaternion.Slerp(transform.rotation, qTo, Time.deltaTime * rotationSpeed);

        if (Input.GetKey(KeyCode.Space) && isGrounded) // Permet de sauter si on est au sol
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        /*if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
        {
            animator.SetBool("isWalking", true);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }*/

        if (Input.GetKey(KeyCode.LeftShift))
        {
            isRunning = true;
            speed = 5;
        }
        else
        {
            isRunning = false;
            speed = 3;
        }

        if(Input.GetKeyDown(KeyCode.F1))
        {
            ScreenCapture.CaptureScreenshot("img" + screenshotIndex + ".png");
            screenshotIndex++;
        }

    }

    void OnCollisionEnter(Collision col) // Permet de resauter lorsqu'on atterit au sol
    {
        if ((col.gameObject.tag == ("Room") || col.gameObject.name == ("roof")) && isGrounded == false)
        {
            isGrounded = true;
        }
        if (col.gameObject.tag == "Zombie")
        {
            speed *= 0f;
            col.gameObject.GetComponent<ZombieMovement>().speed = 0f;
            col.gameObject.GetComponent<ZombieMovement>().animator.SetTrigger("isAttacking");
            Destroy(gameObject);
            var zombiesToStop = GameObject.FindGameObjectsWithTag("Zombie");
            foreach (GameObject zombie in zombiesToStop)
            {
                zombie.GetComponent<ZombieMovement>().speed = 0;
                zombie.GetComponent<ZombieMovement>().animator.SetBool("isPlayerDead", true);
				zombie.GetComponent<ZombieMovement>().target = null;

			}
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Sugar" || other.gameObject.tag == "Water" || other.gameObject.tag == "Food")
        {
            speed += 0.35f;
            TriggerFoodF.TriggerFood(other);
        }
    }
}
