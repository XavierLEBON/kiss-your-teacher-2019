﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreLoader : MonoBehaviour
{
    public float time = 5.0F;
    public GameObject textScore;
    // Start is called before the first frame update
    void Start()
    {
        textScore.GetComponent<TextMeshProUGUI>().text = ScoreStocker.score;
    }

    private void Update()
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            SceneManager.LoadScene("startMenu");
        }
    }
}