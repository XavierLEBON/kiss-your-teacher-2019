﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine;
using System.Threading;

public class TimerF : MonoBehaviour
{
    public GameObject healthBar;
    public GameObject scoreNumber;
    public GameObject foodBar;
    public GameObject sugarBar;
    public GameObject waterBar;
    float time = 10.0F;
    float Damage = 3.0F;
    float set = 5.0F;

    private void Start()
    {
        healthBar = FoodManagerF.Current.HealthBar;
        foodBar = FoodManagerF.Current.foodBar;
        sugarBar = FoodManagerF.Current.sugarBar;
        waterBar = FoodManagerF.Current.waterBar;
        scoreNumber = FoodManagerF.Current.scoreNumber;
    }
    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            Text scoreUpdate = scoreNumber.GetComponent<Text>();
            int score = Convert.ToInt32(scoreUpdate.text);
            score++;
            scoreUpdate.text = score.ToString();
            TriggerFoodF.ImproveDifficulty();
            time = 10;
        }
        set -= Time.deltaTime;
        if (set <= 0)
        {
            int val = 3;
            Text food = foodBar.GetComponent<Text>();
            Text sugar = sugarBar.GetComponent<Text>();
            Text water = waterBar.GetComponent<Text>();

            int foodUpdate = Convert.ToInt32(food.text);
            int sugarUpdate = Convert.ToInt32(sugar.text);
            int waterUpdate = Convert.ToInt32(water.text);

            foodUpdate -= val;
            if (foodUpdate <= 0)
            {
                Text healthUpdate = healthBar.GetComponent<Text>();
                int healthDamage = Convert.ToInt32(healthUpdate.text);
                healthDamage -= 10;
                healthUpdate.text = healthDamage.ToString();
                Damage -= Time.deltaTime;
                foodUpdate = 50;
            }
            sugarUpdate -= val;
            if (sugarUpdate <= 0)
            {
                Text healthUpdate = healthBar.GetComponent<Text>();
                int healthDamage = Convert.ToInt32(healthUpdate.text);
                healthDamage -= 10;
                healthUpdate.text = healthDamage.ToString();
                Damage -= Time.deltaTime;
                sugarUpdate = 50;
            }
            waterUpdate -= val;
            if (waterUpdate <= 0)
            {
                Text healthUpdate = healthBar.GetComponent<Text>();
                int healthDamage = Convert.ToInt32(healthUpdate.text);
                healthDamage -= 10;
                healthUpdate.text = healthDamage.ToString();
                Damage -= Time.deltaTime;
                waterUpdate = 50;
            }
            set = 5;
            food.text = foodUpdate.ToString();
            sugar.text = sugarUpdate.ToString();
            water.text = waterUpdate.ToString();
        }
    }

}
