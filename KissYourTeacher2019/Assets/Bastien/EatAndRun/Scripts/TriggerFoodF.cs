﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TriggerFoodF : MonoBehaviour
{
    public static GameObject healthBar;
    public static GameObject foodBar;
    public static GameObject sugarBar;
    public static GameObject waterBar;
    public static GameObject scoreNumber;

    private void Start()
    {
        healthBar = FoodManagerF.Current.HealthBar;
        foodBar = FoodManagerF.Current.foodBar;
        sugarBar = FoodManagerF.Current.sugarBar;
        waterBar = FoodManagerF.Current.waterBar;
        scoreNumber = FoodManagerF.Current.scoreNumber;
    }

    public static void TriggerFood(Collider other)
    {
            Text scoreUpdate = scoreNumber.GetComponent<Text>();
            int score = Convert.ToInt32(scoreUpdate.text);
            int h = 0;
            switch (other.tag)
            {
                case "Food":
                    //score
                    score += 10;
                    scoreUpdate.text = score.ToString();
                    //Score
                    Text foodUpdate = foodBar.GetComponent<Text>();
                    int food = Convert.ToInt32(foodUpdate.text);
                    int sumF = food + 10;
                    if(sumF > 100)
                    {
                        sumF = 70;
                        foodUpdate.text = sumF.ToString();
                        Text health = healthBar.GetComponent<Text>();
                        h = Convert.ToInt32(health.text);
                        int damage = 10;
                        int dif = h - damage;
                        health.text = dif.ToString();
                }
                    else
                    {
                    
                        foodUpdate.text = sumF.ToString();
                    }
                    break;
                case "Water":
                    //score
                    score += 10;
                    scoreUpdate.text = score.ToString();
                //Score
                    Text waterUpdate = waterBar.GetComponent<Text>();
                    int water = Convert.ToInt32(waterUpdate.text);
                    int sumW = water + 30;
                    if (sumW > 100)
                    {
                        sumW = 70;
                        waterUpdate.text = sumW.ToString();
                        Text health = healthBar.GetComponent<Text>();
                        h = Convert.ToInt32(health.text);
                        int damage = 10;
                        int dif = h - damage;
                        health.text = dif.ToString();
                    }
                    else
                    {
                    Debug.Log("hey i'm here");
                        waterUpdate.text = sumW.ToString();
                    }
                    break;
                case "Sugar":
                    //score
                    score += 10;
                    scoreUpdate.text = score.ToString();
                //Score
                Text sugarUpdate = sugarBar.GetComponent<Text>();
                    int sugar = Convert.ToInt32(sugarUpdate.text);
                    int sumS = sugar + 10;
                    if (sumS > 100)
                    {
                        sumS = 70;
                        sugarUpdate.text = sumS.ToString();
                        Text health = healthBar.GetComponent<Text>();
                        h = Convert.ToInt32(health.text);
                        int damage = 10;
                        int dif = h - damage;
                        health.text = dif.ToString();
                }
                    else
                    {
                        sugarUpdate.text = sumS.ToString();
                    }
                    break;
            }

        ImproveDifficulty();
        Destroy(other.gameObject);
    }

    public static void ImproveDifficulty()
    {
        int score = Convert.ToInt32(scoreNumber.GetComponent<Text>().text);
        ScoreStocker.score = score.ToString();
        int difficulté = Mathf.FloorToInt((11 / 75000.0F) * Mathf.Pow(score, 2) + 1 / 10.0F);
        if (difficulté > 1)
        {
            SpawnZombieF.minNumOfZombie = difficulté;
            SpawnZombieF.maxNumOfZombie = difficulté + 2;
        }
    }
}