﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggeredF : MonoBehaviour
{ 
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.name == "Player")
        {
            Debug.Log("Collision");
            float border = 0.3F;
            float x = col.transform.position.x;
            float z = col.transform.position.z;
            float y = col.transform.position.y;
            int random = Random.Range(0, 2);
            Debug.Log(random);

            if (x < 1.5 && x > -1.5)
            {
                if (z > 0)
                {
                    col.transform.position = new Vector3(x, y, -z + border);
                    Debug.Log(random);
                }


                if (z < 0)
                {
                    col.transform.position = new Vector3(x, y, -z - border);
                    random += 3;
                    Debug.Log(random);
                }
            }

            if (z < 1.5 && z > -1.5)
            {
                
                if (x < 0)
                { 
                   col.transform.position = new Vector3(-x - border, y, z);
                   random += 6;
                    Debug.Log(random);

                }
                if (x > 0)
                {
                    col.transform.position = new Vector3(-x + border, y, z);
                    random += 9;
                    Debug.Log(random);

                }

            }

            RoomsF.switchRoom(random);
        }
    }
}
