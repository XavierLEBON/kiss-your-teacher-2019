﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine;

public class CarenceDamage : MonoBehaviour
{
    public static GameObject healthBar;
    private void Start()
    {
        healthBar = FoodManagerMM.Current.HealthBar;
    }
    public static void DamageCarence()
    {
        Text healthDamaged = healthBar.GetComponent<Text>();
        int healthUpdate = Convert.ToInt32(healthDamaged.text);
        healthUpdate -= 3;
        healthDamaged.text = healthUpdate.ToString();
    }
}
