using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementMM : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    Rigidbody rb;
    public float jumpForce;
    public bool isGrounded = true;
    private Quaternion qTo;
    private Animator animator;
    public bool isRunning;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        qTo = transform.rotation;
        animator = GetComponent<Animator>();
        //animator.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(horizontal * Time.deltaTime * speed, 0, vertical * Time.deltaTime * speed);
        //Vector3 direction = new Vector3(horizontal, 0, vertical);
        //gameObject.transform.Translate(direction.normalized * Time.deltaTime * speed);
        rb.MovePosition(transform.position + movement);
        //transform.rotation = Quaternion.LookRotation(movement);
        if (movement != Vector3.zero) qTo = Quaternion.LookRotation(movement);
        transform.rotation = Quaternion.Slerp(transform.rotation, qTo, Time.deltaTime * rotationSpeed);

        if (Input.GetKey(KeyCode.Space) && isGrounded) // Permet de sauter si on est au sol
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        /*if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
        {
            animator.SetBool("isWalking", true);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }*/

        if (Input.GetKey(KeyCode.LeftShift))
        {
            isRunning = true;
            speed = 6;
        }
        else
        {
            isRunning = false;
            speed = 3;
        }

    }

    void OnCollisionEnter(Collision col) // Permet de resauter lorsqu'on atterit au sol
    {
        if ((col.gameObject.tag == ("Room") || col.gameObject.name == ("roof")) && isGrounded == false)
        {
            isGrounded = true;
        }
        if (col.gameObject.tag == "Zombie")
        {
            speed *= 0f;
            col.gameObject.GetComponent<ZombieMovement>().speed = 0f;
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food"|| other.gameObject.tag == "Water" || other.gameObject.tag == "Sugar")
        {
            speed += 0.35f;
            Debug.Log(other.gameObject.tag + " test before function" );
            TriggerFoodMM.TriggerFood(other);
        }
    }
}
