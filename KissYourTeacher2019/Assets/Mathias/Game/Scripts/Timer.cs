﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public static GameObject healthBar;
    public static GameObject foodBar;
    public static GameObject sugarBar;
    public static GameObject waterBar;
    public GameObject scoreNumber;
    float time = 10.0F;
    public int addScore = 10;

    private void Start()
    {
        scoreNumber = FoodManagerMM.Current.scoreNumber;
        healthBar = FoodManagerMM.Current.HealthBar;
        foodBar = FoodManagerMM.Current.foodBar;
        sugarBar = FoodManagerMM.Current.sugarBar;
        waterBar = FoodManagerMM.Current.waterBar;
    }
    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if(time <= 0)
        {
            Text scoreUpdate = scoreNumber.GetComponent<Text>();
            int score = Convert.ToInt32(scoreUpdate.text);
            score += addScore;
            scoreUpdate.text = score.ToString();
            time = 10;

        }
    }

}
