﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieMovementMM : MonoBehaviour
{
    public GameObject target;
    private Vector3 offset;
    private float minSpeed = 0.035f;
    private float maxSpeed = 0.07f;
    public float speed;
    Rigidbody rb;
    //private Animator animator;
    private bool follow;
    Quaternion targetRotation;
    public float countDownRotation = 2;
    Vector3 targetPoint;

    private void Start()
    {
        gameObject.SetActive(false);
        Invoke("Spawn", 0);
        //animator = GetComponent<Animator>();
    }
    private void Update()
    {
        transform.Translate(Vector3.forward * speed);

        targetPoint = target.transform.position;
        if(target.GetComponent<PlayerMovementMM>().isRunning) // Si le joueur court
        {
            targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up); // Alors on met la rotation en direction du joueur
            speed = maxSpeed;
        }
        else
        {
            if(countDownRotation > 0f) // Si on a changé de sens de rotation récemment
            {
                countDownRotation -= Time.deltaTime;
            }
            else
            {
                speed = minSpeed;
                // On met une rotation aléatoire
                targetRotation = Quaternion.LookRotation(new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f)), Vector3.up);
                countDownRotation = 2;
            }
            
        }
        
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.5f);
    }
    private void Spawn()
    {
        gameObject.SetActive(true);
        speed = minSpeed;
        //transform.position = new Vector3(3, 0.5f, 3);
        transform.rotation = Quaternion.LookRotation(new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f)), Vector3.up);
        //animator.SetBool("isWalking", true);
    }
    void OnCollisionEnter(Collision col) // Si le zombie entre en collision avec un mur
    {
        if ((col.gameObject.tag == ("Room"))) // Si le zombie heurte un mur
        {
            targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up); // On change de rotation en direction du joueur
            countDownRotation = 2;
        }
        else if(col.gameObject.tag == ("Zombie")) // Si le zombie heurte un autre zombie
        {
            targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up); // On change de rotation en direction du joueur
            countDownRotation = 2;
        }
    }
}
