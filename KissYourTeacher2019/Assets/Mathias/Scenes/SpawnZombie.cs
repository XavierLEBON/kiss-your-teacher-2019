﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZombie : MonoBehaviour
{
    public static GameObject zombie;
    public GameObject zombieForInspector;

    public static float radius = 5.0F;
    public float radiusForInspector = radius;

    public static float doorRadius = 9.0F;
    public float doorRadiusForInspector = radius;

    public static int minNumOfZombie;
    public static int maxNumOfZombie;
    public int minNumOfZombieForInspector;
    public int maxNumOfZombieForInspector;

    private void Awake()
    {
        zombie = zombieForInspector;
        radius = radiusForInspector;
        doorRadius = doorRadiusForInspector;
        minNumOfZombie = minNumOfZombieForInspector;
        maxNumOfZombie = maxNumOfZombieForInspector;
    }

    static protected void PlaceZombie(int numOfZombie, Vector2 entranceDoorPosition)
    {
        Vector2 actualZombie;
        Vector4[] noSpawnAreas = new Vector4[numOfZombie+1];

        noSpawnAreas[0] = new Vector4(entranceDoorPosition.x - doorRadius, entranceDoorPosition.x + doorRadius, entranceDoorPosition.y - doorRadius, entranceDoorPosition.y + doorRadius);

        bool isInRange;
        for (int i = 0; i < numOfZombie; i++)
        {
            isInRange = false;
            float x = Random.Range(-7.5F, 7.5F);
            float z = Random.Range(-4.5F, 4.5F);

            actualZombie = new Vector2(x, z);
            for (int j = 0; j <= i; j++)
            {
                if (actualZombie.x >= noSpawnAreas[j].w && actualZombie.x <= noSpawnAreas[j].x && actualZombie.y >= noSpawnAreas[j].y && actualZombie.y <= noSpawnAreas[j].z)
                {
                    isInRange = true;
                    i--;
                }
            }

            if (!isInRange)
            {
                Instantiate(zombie, new Vector3(x, -0.3F, z), Quaternion.identity);
                noSpawnAreas[i+1] = new Vector4(actualZombie.x - radius, actualZombie.x + radius, actualZombie.y - radius, actualZombie.y + radius);
            }
        }
    }

    public static void InstantiateZombie(Vector2 entranceDoorPosition)
    {
        //How many food will be generated
        int numOfZombie = Random.Range(minNumOfZombie, maxNumOfZombie);
        PlaceZombie(numOfZombie, entranceDoorPosition);
    }
}
