﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger2 : MonoBehaviour
{
    public List<GameObject> rooms;
    private void OnTriggerEnter(Collider col)
    {
        if (col.name == "Player")
        {
            float x = col.transform.position.x;
            float z = col.transform.position.z;

            if (x < 1 && x > -1)
            {
                if (z < 0)
                    col.transform.position = new Vector3(x, 1, -z - 1);
                if (z > 0)
                    col.transform.position = new Vector3(x, 1, -z + 1);

            }

            if (z < 1 && z > -1)
            {
                if (x < 0)
                    col.transform.position = new Vector3(-x - 1, 1, z);
                if (x > 0)
                    col.transform.position = new Vector3(-x + 1, 1, z);

            }


        }
    }
}
