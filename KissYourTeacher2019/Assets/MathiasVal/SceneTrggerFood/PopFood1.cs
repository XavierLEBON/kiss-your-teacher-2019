﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopFood1 : MonoBehaviour
{
    //variable
    public GameObject[] allItem = new GameObject[5];
    private GameObject[] tmp = new GameObject[5];
    private GameObject[] itemSelected = new GameObject[5];
    private const float length = 4;
    public GameObject ground;

    private Vector3 mapLimits;
    private Vector3 mapOrigins;

    private float radius = 1.5F;
    private Vector2 actualItem;
    private Vector4[] itemsRadius = new Vector4[4];
    private bool isInRange;

    public string debug;

    //assignation


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnFood());
    }

    private IEnumerator SpawnFood()
    {
        yield return new WaitForSeconds(.5f);
        //copy allItems into tmp
        for (int i = 0; i < allItem.Length; i++)
        {
            tmp[i] = allItem[i].gameObject;
        }

        //instanciate the ground
        Instantiate(ground);
        /* rand and "drag drop" into itemSelected and update TMP tab */
        for (int i = 0; i < length; i++)
        {
            var rand = Random.Range(0, tmp.Length);
            if (tmp[rand] != null)
            {
                itemSelected[i] = tmp[rand];
                tmp[rand] = null;
            }
            else
            {
                i--;
            }
        }
        // instanciate itemSelected into the map 

        //Définition de la taille de la map (hors-mur)
        mapOrigins = new Vector3(-4.75F, -0.3F, 4.75F);
        mapLimits = new Vector3(4.75F, -0.3F, -4.75F);


        for (int i = 0; i < length; i++)
        {
            isInRange = false;
            float x = Random.Range(-4.75F, 4.75F);
            float z = Random.Range(-4.75F, 4.75F);

            actualItem = new Vector2(x, z);
            for (int j = 0; j < i; j++)
            {
                if (actualItem.x >= itemsRadius[j].w && actualItem.x <= itemsRadius[j].x && actualItem.y >= itemsRadius[j].y && actualItem.y <= itemsRadius[j].z)
                {
                    isInRange = true;
                    i--;
                }
            }

            if (!isInRange)
            {
                Instantiate(itemSelected[i], new Vector3(x, -0.3F, z), Quaternion.identity);
                itemsRadius[i] = new Vector4(actualItem.x - radius, actualItem.x + radius, actualItem.y - radius, actualItem.y + radius);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
