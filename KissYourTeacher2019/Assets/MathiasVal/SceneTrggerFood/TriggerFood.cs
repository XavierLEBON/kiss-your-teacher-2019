﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TriggerFood : MonoBehaviour
{
    public GameObject healthBar;
    public GameObject foodBar;
    public GameObject sugarBar;
    public GameObject waterBar;
    public GameObject scoreNumber;
    private int add = 10;

    private void Start()
    {
        healthBar = FoodManager.Current.HealthBar;
        foodBar = FoodManager.Current.foodBar;
        sugarBar = FoodManager.Current.sugarBar;
        waterBar = FoodManager.Current.waterBar;
        scoreNumber = FoodManager.Current.scoreNumber;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Sphere")
        {
            Text scoreUpdate = scoreNumber.GetComponent<Text>();
            int score = Convert.ToInt32(scoreUpdate.text);
            switch (this.tag)
            {
                case "Food":
                    //score
                    score += 10;
                    scoreUpdate.text = score.ToString();
                    //Score
                    Text foodUpdate = foodBar.GetComponent<Text>();
                    int food = Convert.ToInt32(foodUpdate.text);
                    int sumF = food + add;
                    if(sumF > 100)
                    {
                        sumF = 70;
                        foodUpdate.text = sumF.ToString();
                        //Text health = healthBar.GetComponent<Text>();
                        //int h = Convert.ToInt32(health);
                        //int damage = 10;
                        //int dif = h - damage;
                        //health.text = dif.ToString();
                    }
                    else
                    {
                        foodUpdate.text = sumF.ToString();
                    }
                    break;
                case "Water":
                    //score
                    score += 10;
                    scoreUpdate.text = score.ToString();
                    //Score
                    Text waterUpdate = waterBar.GetComponent<Text>();
                    int water = Convert.ToInt32(waterUpdate);
                    int sumW = water + add;
                    if (sumW > 100)
                    {
                        sumW = 70;
                        waterUpdate.text = sumW.ToString();
                        //Text health = healthBar.GetComponent<Text>();
                        //int h = Convert.ToInt32(health);
                        //int damage = 10;
                        //int dif = h - damage;
                        //health.text = dif.ToString();
                    }
                    else
                    {
                        waterUpdate.text = sumW.ToString();
                    }
                    break;
                case "Sugar":
                    //score
                    score += 10;
                    scoreUpdate.text = score.ToString();
                    //Score
                    Text sugarUpdate = sugarBar.GetComponent<Text>();
                    int sugar = Convert.ToInt32(sugarUpdate);
                    int sumS = sugar + add;
                    if (sumS > 100)
                    {
                        sumS = 70;
                        sugarUpdate.text = sumS.ToString();
                        //Text health = healthBar.GetComponent<Text>();
                        //int h = Convert.ToInt32(health);
                        //int damage = 10;
                        //int dif = h - damage;
                        //health.text = dif.ToString();
                    }
                    else
                    {
                        sugarUpdate.text = sumS.ToString();
                    }
                    break;
            }
            Destroy(this.gameObject);

        }

    }
}