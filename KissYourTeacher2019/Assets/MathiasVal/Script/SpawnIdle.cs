﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnIdle : MonoBehaviour
{
    //variable
    public GameObject[] allItem = new GameObject[5];
    private GameObject[] tmp = new GameObject[5];
    private GameObject[] itemSelected = new GameObject[5];
    private int length;
    public GameObject ground;
    private Vector3 mapSize;
    private Vector3 mapLimits;
    private Vector3 mapOrigins;
    private float radius;

    // Start is called before the first frame update
    void Start()
    {
        length =  Random.Range(1, 4);
        //copy allItems into tmp
        for (int i = 0; i < allItem.Length; i++)
        {
            tmp[i] = allItem[i].gameObject;
        }
        //instanciate the ground
        Instantiate(ground);
        /* rand and "drag drop" into itemSelected and update TMP tab */
        for (int i = 0; i < length; i++)
        {
            var rand = Random.Range(0, tmp.Length);
            if(tmp[rand] != null)
            {
                itemSelected[i] = tmp[rand];
                tmp[rand] = null;
            }
            else
            {
                i--;
            }
        }
        //Définition de la taille de la map (hors-mur)
        mapOrigins = new Vector3(-4.75F, -0.3F, 4.75F);
        mapLimits = new Vector3(4.75F, -0.3F, -4.75F);
        //random instance
        for (int i = 0; i < length; i++)
        {
            float x = Random.Range(-4.75F, 4.75F);
            float z = Random.Range(-4.75F, 4.75F);
            Instantiate(itemSelected[i], new Vector3(x, -0.3F, z), Quaternion.identity);
        }
    }
}
