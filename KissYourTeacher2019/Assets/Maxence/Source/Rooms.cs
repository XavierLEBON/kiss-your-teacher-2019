﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rooms : MonoBehaviour
{
    public static List<GameObject> roomsList;
    public List<GameObject> roomsListForInspector;
    public static GameObject[] foodsToDestroy;

    public static int actualRoom;
    public int actualRoomForInspector;
    // Start is called before the first frame update

    private void Start()
    {
        roomsList = roomsListForInspector;
        actualRoom = actualRoomForInspector;
        roomsList[actualRoom].SetActive(true);
        getEntranceDoorPosition(actualRoom);
        SpawnFoodF.InstantiateFood();
    }

    public static void switchRoom(int random)
    {
        if (actualRoom != random)
        {
            roomsList[random].SetActive(true);
            roomsList[actualRoom].SetActive(false);
            actualRoom = random;
        }
        foodsToDestroy = GameObject.FindGameObjectsWithTag("Food");
        foreach (GameObject foodToDestroy in foodsToDestroy)
            Destroy(foodToDestroy);
        SpawnFoodF.InstantiateFood();
    }

    public static Vector2 getEntranceDoorPosition(int mapNumber)
    {
        GameObject entranceDoor = roomsList[mapNumber].transform.GetChild(1).gameObject;
        if(entranceDoor.name != "Prefab_Entrance_Door")
        {
            entranceDoor = roomsList[mapNumber].transform.GetChild(0).gameObject;
        }
        Vector3 entranceDoorPosition = entranceDoor.transform.position;
        return new Vector2(entranceDoorPosition.x, entranceDoorPosition.z);
    }

}
