﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFood : MonoBehaviour
{
    //variable
    public static GameObject[] allItems;
    public List<GameObject> allItemsForInspector;
    public static float radius = 5.0F;
    public float radiusForInspector = radius;
    
    private void Awake()
    {
        allItems = allItemsForInspector.ToArray();
        radius = radiusForInspector;
    }

    

    static protected GameObject[] CreateTempFoodArray()
    {
        GameObject[] tmp = new GameObject[allItems.Length];
        for (int i = 0; i < allItems.Length; i++)
        {
            tmp[i] = allItems[i].gameObject;
        }

        return tmp;
    }

    static protected GameObject[] FoodToGenerate(GameObject[] tempFoodArray, int numOfFood){
        /* rand and "drag drop" into itemSelected and update TMP tab */
        GameObject[] itemSelected = new GameObject[numOfFood];

        for (int i = 0; i<numOfFood; i++)
        {
            var rand = Random.Range(0, tempFoodArray.Length);
            if (tempFoodArray[rand] != null)
            {
                itemSelected[i] = tempFoodArray[rand];
                tempFoodArray[rand] = null;
            }
            else
            {
                i--;
            }
        }
        return itemSelected;
    }

    static protected void PlaceFood(GameObject[] foodToGenerate, int numOfFood)
    {
        Vector2 actualItem;
        Vector4[] itemsRadius = new Vector4[4];
        bool isInRange;
        for (int i = 0; i < numOfFood; i++)
        {
            isInRange = false;
            float x = Random.Range(-7.5F, 7.5F);
            float z = Random.Range(-4.5F, 4.5F);

            actualItem = new Vector2(x, z);
            for (int j = 0; j < i; j++)
            {
                if (actualItem.x >= itemsRadius[j].w && actualItem.x <= itemsRadius[j].x && actualItem.y >= itemsRadius[j].y && actualItem.y <= itemsRadius[j].z)
                {
                    isInRange = true;
                    i--;
                }
            }

            if (!isInRange)
            {
                Instantiate(foodToGenerate[i], new Vector3(x, -0.3F, z), Quaternion.identity);
                itemsRadius[i] = new Vector4(actualItem.x - radius, actualItem.x + radius, actualItem.y - radius, actualItem.y + radius);
            }
        }
    }

     public static void InstantiateFood()
    {
        //How many food will be generated
        int numOfFood = Random.Range(1, 4);
        //copy allItems into tmp
        GameObject[] tmp = CreateTempFoodArray();
        // instantiate itemSelected into the map 
        GameObject[] itemSelected = FoodToGenerate(tmp, numOfFood);
        PlaceFood(itemSelected, numOfFood);
    }
}
