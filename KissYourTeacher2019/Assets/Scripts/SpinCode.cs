﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinCode : MonoBehaviour
{
    // Start is called before the first frame update
    private float velocity = 250f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, velocity * Time.deltaTime);
    }
}
